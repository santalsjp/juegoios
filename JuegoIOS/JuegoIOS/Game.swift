//
//  Game.swift
//  JuegoIOS
//
//  Created by Santiago Pazmiño on 13/11/17.
//  Copyright © 2017 Santiago Pazmiño. All rights reserved.
//

import Foundation

class Game {
    
   private var score:Int
   private var target:Int
   private var round:Int
    
    init() {
        score = 0
        round = 1
        target = Int(arc4random_uniform(99) + 1)
    }
    
    func getValue() -> (score:Int, target:Int, round:Int){
        return (score,target,round)
    }
    func restar(){
        score = 0
        round = 1
        target = Int(arc4random_uniform(99) + 1)
    }
    func play(sliderValue:Int) -> Int{
        
        var difference = target - sliderValue
        
        if difference < 0 {
            difference *= -1
        }
        
        round += 1
        target = Int(arc4random_uniform(99) + 1)
        
        print(difference)
        
        switch difference {
        case 0:
            score += 100
            return 100
        case 1...3:
            score += 75
            return 75
        case 4...10:
            score += 50
            return 50
        default:
            return 0
        }
        
        
        
    }
    
}
