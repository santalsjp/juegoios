//
//  ViewController.swift
//  JuegoIOS
//
//  Created by Santiago Pazmiño on 13/11/17.
//  Copyright © 2017 Santiago Pazmiño. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var numAleatorio: UILabel!
    
    
    @IBOutlet weak var numScore: UILabel!
    
    
    @IBOutlet weak var numRonda: UILabel!
    
    @IBOutlet weak var sliderLabel: UISlider!
    
    let game = Game()
    
    @IBAction func btnJugar(_ sender: Any) {
        let sliderValue = Int(round(sliderLabel.value))
        let ScoreRound = game.play(sliderValue: sliderValue)
        
        let alertController = UIAlertController(title: "Het!", message: "El valor que sacaste fue: \(sliderValue) y tu puntaje es: \(ScoreRound)", preferredStyle:.actionSheet)
        let accepAction = UIAlertAction(title: "Aceptar", style: .default) { (action) in
           self.updateLoad()
        }
        alertController.addAction(accepAction)
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnReiniciar(_ sender: Any) {
       game.restar()
        updateLoad()
        sliderLabel.value = 50
        
    }
    
    func updateLoad(){
        let (gScore, gTarget, gRound) = game.getValue()
        
        numAleatorio.text = "\(gTarget)"
        numRonda.text = "\(gRound)"
        numScore.text = "\(gScore)"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        updateLoad()
        
    }
    
    


}

